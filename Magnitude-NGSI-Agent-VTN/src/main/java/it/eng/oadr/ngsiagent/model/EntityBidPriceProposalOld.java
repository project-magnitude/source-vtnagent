package it.eng.oadr.ngsiagent.model;

/**
 * Entity Model
 */
public class EntityBidPriceProposalOld {

	private AttributeOadrPayload mesId;
	private AttributeOadrPayload bidId;
	private AttributeOadrPayload dateStart;
	private AttributeOadrPayload duration;
	private AttributeOadrPayload uId;
	private AttributeOadrPayload price;

	public EntityBidPriceProposalOld() {
		super();
	}


	public EntityBidPriceProposalOld(AttributeOadrPayload mesId, AttributeOadrPayload bidId,
			AttributeOadrPayload dateStart, AttributeOadrPayload duration, AttributeOadrPayload uId,
			AttributeOadrPayload price) {
		this.mesId = mesId;
		this.bidId = bidId;
		this.dateStart = dateStart;
		this.duration = duration;
		this.uId = uId;
		this.price = price;
	}

	public AttributeOadrPayload getDateStart() {
		return dateStart;
	}

	public void setDateStart(AttributeOadrPayload dateStart) {
		this.dateStart = dateStart;
	}

	public AttributeOadrPayload getDuration() {
		return duration;
	}

	public void setDuration(AttributeOadrPayload duration) {
		this.duration = duration;
	}

	public AttributeOadrPayload getMesId() {
		return mesId;
	}

	public void setMesId(AttributeOadrPayload mesId) {
		this.mesId = mesId;
	}

	public AttributeOadrPayload getBidId() {
		return bidId;
	}

	public void setBidId(AttributeOadrPayload bidId) {
		this.bidId = bidId;
	}

	public AttributeOadrPayload getuId() {
		return uId;
	}

	public void setuId(AttributeOadrPayload uId) {
		this.uId = uId;
	}

	public AttributeOadrPayload getPrice() {
		return price;
	}

	public void setPrice(AttributeOadrPayload price) {
		this.price = price;
	}

}
