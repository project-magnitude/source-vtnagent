package it.eng.oadr.ngsiagent.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TokenPEP {

	private static final Logger log = LoggerFactory.getLogger(TokenPEP.class);

	private static String TOKEN_PEP_CURRENT;
	//private static boolean USE_TOKEN_PEP;

	public static synchronized String getCurrentTokenPEP() {

		return TOKEN_PEP_CURRENT;

	}

	public static synchronized boolean isUseTokenPEP() {

		String stringUseProxyPEP = "";
		try {
			stringUseProxyPEP = AgentProperties.getProperty("UseProxyPEP");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Boolean.parseBoolean(stringUseProxyPEP);

	}

	public static synchronized void generateNewTokenPEP() {
		log.debug("--> generateNewTokenPEP");

		String newTokenPEP = "";
		String authCode = "";

		try {

			URL url = new URL(AgentProperties.getProperty("UrlIdM") + "/oauth2/token");
			log.debug("UrlIdM: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			String clientID = AgentProperties.getProperty("ClientID");
			log.debug("clientID: " + clientID);

			String clientSecret = AgentProperties.getProperty("ClientSecret");
			log.debug("clientSecret: " + clientSecret);

			String stringToEncode = clientID + ":" + clientSecret;
			log.debug("stringToEncode: " + stringToEncode);
			String base64Encoded = Base64.getEncoder().encodeToString(stringToEncode.getBytes());
			log.debug("base64Encoded: " + base64Encoded);
			authCode = "Basic" + " " + base64Encoded;
			log.debug("authCode: " + authCode);
			connService.setRequestProperty("Authorization", authCode);

			String usernameIdm = AgentProperties.getProperty("UsernameIdM");
			log.debug("usernameIdm: " + usernameIdm);
			String passwordIdm = AgentProperties.getProperty("PasswordIdM");
			log.debug("passwordIdm: " + passwordIdm);

			String data = "username=" + usernameIdm + "&password=" + passwordIdm
					+ "&grant_type=password&scope=permanent";
			log.debug("Rest Data: " + data);

			OutputStream os = connService.getOutputStream();
			os.write(data.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			String response = sb.toString();
			JsonObject responseJson = (JsonObject) new JsonParser().parse(response);
			newTokenPEP = responseJson.get("access_token").toString().replace("\"", "");
			log.debug("responseToken: " + newTokenPEP);

			connService.disconnect();

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		// generate new token
		TOKEN_PEP_CURRENT = newTokenPEP;

		log.debug("<-- generateNewTokenPEP");
		return;

	}

}
