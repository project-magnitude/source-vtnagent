package it.eng.oadr.ngsiagent.services;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.oasis_open.docs.ns.energyinterop._201110.EiResponseType;
import org.oasis_open.docs.ns.energyinterop._201110.IntervalType;
import org.oasis_open.docs.ns.energyinterop._201110.PayloadFloatType;
import org.oasis_open.docs.ns.energyinterop._201110.ReportPayloadType;
import org.oasis_open.docs.ns.energyinterop._201110.SignalPayloadType;
import org.openadr.oadr_2_0b._2012._07.OadrDistributeEventType.OadrEvent;
import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrReportType;
import org.openadr.oadr_2_0b._2012._07.OadrResponseType;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ietf.params.xml.ns.icalendar_2_0.stream.StreamPayloadBaseType;
import it.eng.oadr.ngsiagent.model.AttributeOadrPayload;
import it.eng.oadr.ngsiagent.model.EntityBidPriceProposalNew;
import it.eng.oadr.ngsiagent.model.EntityBidPriceProposalOld;
import it.eng.oadr.ngsiagent.model.EntityBidResultNew;
import it.eng.oadr.ngsiagent.model.EntityBidResultOld;
import it.eng.oadr.ngsiagent.model.EntityNGSI;
import it.eng.oadr.ngsiagent.model.EntityOadrPayload;
import it.eng.oadr.ngsiagent.utilities.AgentProperties;
import it.eng.oadr.ngsiagent.utilities.Counters;
import it.eng.oadr.ngsiagent.utilities.TokenPEP;
import it.eng.oadr.openadrmodel.Parser;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
@Path("Simple/2.0b")
public class Simple20b {

	private static final Logger log = LoggerFactory.getLogger(Simple20b.class);
	private static final int HTTP_error_code_Unauthorized = 401;

	@POST
	@Path("EiRegisterParty")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiRegisterParty(String oadrPayload) {
		log.debug("-> postEiRegisterParty");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiRegisterPartyToVEN" + Integer.toString(Counters.getNewMsgEiRegisterParty());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiRegisterPartyToVEN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiRegisterParty");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiRegisterParty");
		return ret;
	}

	@POST
	@Path("EiEvent")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiEvent(String oadrPayload) {
		log.debug("-> postEiEvent");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		sendEntityBidPriceProposal(in);

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiEventToVEN" + Integer.toString(Counters.getNewMsgEiEvent());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiEventToVEN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiEvent");

		// Intercetta l'evento con signalName = "x-BID_PRICE_PROPOSAL"
		// Inizializza la Entità di tipo "BidPriceProposal"
		// Estrae i campi generali dall'evento e valorizza gli attributi della entità
		// Per ogni interval estrae i campi valorizza gli attributi della entità
		// e invia l'entità al Contex Broker

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiEvent");
		return ret;
	}

	@POST
	@Path("EiReport")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiReport(String oadrPayload) {
		log.debug("-> postEiReport");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		sendEntityBidResult(in);
		
		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiReportToVEN" + Integer.toString(Counters.getNewMsgEiReport());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiReportToVEN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiReport");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiReport");
		return ret;
	}

	@POST
	@Path("EiOpt")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiOpt(String oadrPayload) {
		log.debug("-> postEiOpt");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiOptToVEN" + Integer.toString(Counters.getNewMsgEiOpt());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiOptToVEN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiOpt");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiOpt");
		return ret;
	}

	@POST
	@Path("OadrPoll")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postOadrPoll(String oadrPayload) {
		log.debug("-> postOadrPoll");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgOadrPollToVEN" + Integer.toString(Counters.getNewMsgOadrPoll());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgOadrPollToVEN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/OadrPoll");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postOadrPoll");
		return ret;
	}

	public OadrResponseType createResponseType() {
		log.debug("-> createResponseType");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		try {
			ret.setSchemaVersion(AgentProperties.getProperty("schemaVersion"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("<- createResponseType");
		return ret;
	}

	
	public void patchEntityContextBroker(Object myEntity, String myId, String fiwareServicePath) {

		log.debug("-> patchEntityContextBroker");
		log.debug("myEntity:" + myEntity);
		log.debug("myEntity:" + myId);
		log.debug("fiwareServicePath:" + fiwareServicePath);
		boolean tokenWasUnauthorized;

		try {

			do {
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities/" + myId + "/attrs");
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				GsonBuilder gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();
				String record = gson.toJson(myEntity);
				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HTTP_error_code_Unauthorized)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);
				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- patchEntityContextBroker");
		return;
	}
	
	public void postEntityContextBroker(EntityNGSI myEntity, String fiwareServicePath) {

		log.debug("-> postEntityContextBroker");
		log.debug("myEntity:" + myEntity);
		log.debug("fiwareServicePath:" + fiwareServicePath);
		boolean tokenWasUnauthorized;

		try {

			do {
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities");
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				GsonBuilder gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();
				String record = gson.toJson(myEntity);
				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HTTP_error_code_Unauthorized)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);
				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- postEntityContextBroker");
		return;
	}

	public synchronized String encodeOadrPayload(String oadrPayload) {
		log.debug("-> encodeOadrPayload");
		log.debug("oadrPayload: " + oadrPayload);
		String ret = oadrPayload;

		ret = ret.replace("<", "#60$");
		ret = ret.replace(">", "#62$");
		ret = ret.replace("\"", "#34$");
		ret = ret.replace("\'", "#39$");
		ret = ret.replace("=", "#61$");
		ret = ret.replace(";", "#59$");
		ret = ret.replace("(", "#40$");
		ret = ret.replace(")", "#41$");

		log.debug("<- encodeOadrPayload");
		return ret;
	}

	public void sendEntityBidPriceProposal(OadrPayload in) {
		log.debug("-> sendEntityBidPriceProposal");
		log.debug("in: " + in);

		OadrSignedObject outSiOb = in.getOadrSignedObject();

		log.debug("oadrDistributeEvent: " + outSiOb.getOadrDistributeEvent());

		if (in != null && outSiOb != null && outSiOb.getOadrDistributeEvent() != null) {

			OadrEvent myOadrEvent = outSiOb.getOadrDistributeEvent().getOadrEvent().get(0);

			// Intercetta l'evento con signalName = "x-BID_PRICE_PROPOSAL"
			String mySignalName = myOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal().get(0)
					.getSignalName();

			log.debug("mySignalName: " + mySignalName);
			if (mySignalName.equals("x-BID_PRICE_PROPOSAL")) {

				// Inizializza la Entità di tipo "x-FLEXIBILITY_OFFERED"
				//EntityBidPriceProposal myEntityBidPriceProposal = new EntityBidPriceProposal();
				EntityBidPriceProposalNew myEntityBidPriceProposal = new EntityBidPriceProposalNew();

				// Estrae i campi generali dal report e valorizza gli attributi della entità
				
				myEntityBidPriceProposal.setType("BidPriceProposal");

				// Estrae l'info mesId
				String myVtnComment = myOadrEvent.getEiEvent().getEventDescriptor().getVtnComment();
				log.debug("myVtnComment: " + myVtnComment);
				String[] splitVtnComment = myVtnComment.split(",");
				String myMesIdZone = splitVtnComment[0];
				String[] splitMesIdZone = myMesIdZone.split(":");
				String myMesId = splitMesIdZone[1];
				log.debug("myMesId: " + myMesId);
				AttributeOadrPayload myAttributeMesId = new AttributeOadrPayload();
				myAttributeMesId.setType("String");
				myAttributeMesId.setValue(myMesId);
				myEntityBidPriceProposal.setMesId(myAttributeMesId);

				// Estrae l'info bidId
				String myBidIdZone = splitVtnComment[1];
				String[] splitBidIdZone = myBidIdZone.split(":");
				String myBidId = splitBidIdZone[1];
				log.debug("myBidId: " + myBidId);
				AttributeOadrPayload myAttributeBidId = new AttributeOadrPayload();
				myAttributeBidId.setType("String");
				myAttributeBidId.setValue(myBidId);
				myEntityBidPriceProposal.setBidId(myAttributeBidId);

				// Estrae l'info dateStart
				AttributeOadrPayload myAttributeDateStart = new AttributeOadrPayload();
				myAttributeDateStart.setType("DateTime");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				String myDateStart = sdf.format(myOadrEvent.getEiEvent().getEiActivePeriod().getProperties()
						.getDtstart().getDateTime().toGregorianCalendar().getTime());
				log.debug("myDateStart: " + myDateStart);
				myAttributeDateStart.setValue(myDateStart);
				myEntityBidPriceProposal.setDateStart(myAttributeDateStart);

				List<IntervalType> myListIntervalType = myOadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal()
						.get(0).getIntervals().getInterval();

				for (IntervalType myinterval : myListIntervalType) {

					// Estrae l'info duration
					AttributeOadrPayload myAttributeDuration = new AttributeOadrPayload();
					myAttributeDuration.setType("String");
					String myDuration = myinterval.getDuration().getDuration();
					log.debug("myDuration: " + myDuration);
					myAttributeDuration.setValue(myDuration);
					myEntityBidPriceProposal.setDuration(myAttributeDuration);

					// Estrae l'info uId
					AttributeOadrPayload myAttributeuId = new AttributeOadrPayload();
					myAttributeuId.setType("String");
					String myuId = myinterval.getUid().getText();
					log.debug("myuId: " + myuId);
					myAttributeuId.setValue(myuId);
					myEntityBidPriceProposal.setuId(myAttributeuId);

					// Estrae l'info price
					AttributeOadrPayload myAttributePrice = new AttributeOadrPayload();
					myAttributePrice.setType("float");

					List<JAXBElement<? extends StreamPayloadBaseType>> myListSignalPayload = myinterval.getStreamPayloadBase();

					JAXBElement<SignalPayloadType> myJAXBSignalPayloadType = (JAXBElement<SignalPayloadType>) myListSignalPayload.get(0);

					SignalPayloadType mySignalPayloadType = myJAXBSignalPayloadType.getValue();

					JAXBElement<PayloadFloatType> payloadFloatTypePrice = (JAXBElement<PayloadFloatType>) mySignalPayloadType.getPayloadBase();
					float valueFloatPrice = payloadFloatTypePrice.getValue().getValue();
					String myPrice = Float.toString(valueFloatPrice);
					log.debug("myPrice: " + myPrice);
					myAttributePrice.setValue(myPrice);
					myEntityBidPriceProposal.setPrice(myAttributePrice);

					// invia l'Entità NGSI al Context Broker
//					String idEntityBidPriceProposal = "BidPriceProposal"
//							+ Integer.toString(Counters.getNewBidPriceProposal());
					String idEntityBidPriceProposal = "BidPriceProposalList";
					log.debug("idEntityBidPriceProposal: " + idEntityBidPriceProposal);
					myEntityBidPriceProposal.setId(idEntityBidPriceProposal);
					//postEntityContextBroker(myEntityBidPriceProposal, "/BidPriceProposal");
					if (Counters.existsBidPriceProposal()) {
						EntityBidPriceProposalOld myEntityBidPriceProposalOld = new EntityBidPriceProposalOld(myEntityBidPriceProposal.getMesId(),
																											  myEntityBidPriceProposal.getBidId(),
																											  myEntityBidPriceProposal.getDateStart(),
																											  myEntityBidPriceProposal.getDuration(),
																											  myEntityBidPriceProposal.getuId(),
																											  myEntityBidPriceProposal.getPrice());
						
						patchEntityContextBroker(myEntityBidPriceProposalOld, myEntityBidPriceProposal.getId(), "/BidPriceProposal");
					} else {
						postEntityContextBroker(myEntityBidPriceProposal, "/BidPriceProposal");	
						Counters.setBidPriceProposal();
					}
				}
			}

		}

		log.debug("<- sendEntityBidPriceProposal");
		return;
	}
	public void sendEntityBidResult(OadrPayload in) {
		log.debug("-> sendEntityBidResult");
		log.debug("in: " + in);

		// Intercetta il report con reportName = "x-BID_RESULT"
		OadrSignedObject outSiOb = in.getOadrSignedObject();
		log.debug("OadrUpdateReport: " + outSiOb.getOadrUpdateReport());
		if (in != null && outSiOb != null && outSiOb.getOadrUpdateReport() != null) {
			OadrReportType myOadrReport = outSiOb.getOadrUpdateReport().getOadrReport().get(0);
			log.debug("ReportName: " + myOadrReport.getReportName());
			if (myOadrReport.getReportName().equals("x-BID_RESULT")) {
				// Inizializza la Entità di tipo "x-FLEXIBILITY_OFFERED"
				
				
				//EntityBidResult myEntityBidResult = new EntityBidResult();
				EntityBidResultNew myEntityBidResult = new EntityBidResultNew();
				// Estrae i campi generali dal report e valorizza gli attributi della entità
//				String idEntityBidResult = "BidResult"
//						+ Integer.toString(Counters.getNewBidResult());
				String idEntityBidResult = "BidResultList";
				log.debug("idEntityBidResult: " + idEntityBidResult);
				myEntityBidResult.setId(idEntityBidResult);
				myEntityBidResult.setType("BidResult");

				List<IntervalType> myListIntervalType = myOadrReport.getIntervals().getInterval();

				for (IntervalType myinterval : myListIntervalType) {
					AttributeOadrPayload myAttributeResourceId = new AttributeOadrPayload();
					myAttributeResourceId.setType("String");
					JAXBElement<ReportPayloadType> myJAXBReportPayloadType = (JAXBElement<ReportPayloadType>) myinterval
							.getStreamPayloadBase().get(0);
					ReportPayloadType myReportPayloadType = myJAXBReportPayloadType.getValue();
					String myRID = myReportPayloadType.getRID();
					String[] splitRID = myRID.split("_");
					String myResourceId = splitRID[0];
					log.debug("myResourceId: " + myResourceId);
					myAttributeResourceId.setValue(myResourceId);
					myEntityBidResult.setResourceId(myAttributeResourceId);

					AttributeOadrPayload myAttributeDateStart = new AttributeOadrPayload();
					myAttributeDateStart.setType("DateTime");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					String myDateStart = sdf
							.format(myinterval.getDtstart().getDateTime().toGregorianCalendar().getTime());
					log.debug("myDateStart: " + myDateStart);
					myAttributeDateStart.setValue(myDateStart);
					myEntityBidResult.setDateStart(myAttributeDateStart);

					AttributeOadrPayload myAttributeDuration = new AttributeOadrPayload();
					myAttributeDuration.setType("String");
					String myDuration = myinterval.getDuration().getDuration();
					log.debug("myDuration: " + myDuration);
					myAttributeDuration.setValue(myDuration);
					myEntityBidResult.setDuration(myAttributeDuration);

					List<JAXBElement<? extends StreamPayloadBaseType>> myListOadrReportPayload = myinterval
							.getStreamPayloadBase();

					for (Object myOadrReportPayload : myListOadrReportPayload) {
						JAXBElement<ReportPayloadType> myJAXBReportPayloadType2 = (JAXBElement<ReportPayloadType>) myOadrReportPayload;
						ReportPayloadType myReportPayloadType2 = myJAXBReportPayloadType2.getValue();
						String myRID2 = myReportPayloadType2.getRID();
						int myIndex = myResourceId.length() + 1;
						String mySuffix = myRID2.substring(myIndex);
						log.debug("mySuffix: " + mySuffix);
						switch (mySuffix) {
						case "flexibility_price":
							AttributeOadrPayload myAttributeFlexibilityPrice = new AttributeOadrPayload();
							myAttributeFlexibilityPrice.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityPrice = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityPrice = payloadFloatTypeFlexibilityPrice.getValue().getValue();
							String myFlexibilityPrice = Float.toString(valueFloatFlexibilityPrice);
							log.debug("myFlexibilityPrice: " + myFlexibilityPrice);
							myAttributeFlexibilityPrice.setValue(myFlexibilityPrice);
							myEntityBidResult.setFlexibilityPrice(myAttributeFlexibilityPrice);
							break;
						case "flexibility_offered":
							AttributeOadrPayload myAttributeFlexibilityOffered = new AttributeOadrPayload();
							myAttributeFlexibilityOffered.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityOffered = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityOffered = payloadFloatTypeFlexibilityOffered.getValue()
									.getValue();
							String myFlexibilityOffered = Float.toString(valueFloatFlexibilityOffered);
							log.debug("myFlexibilityOffered: " + myFlexibilityOffered);
							myAttributeFlexibilityOffered.setValue(myFlexibilityOffered);
							myEntityBidResult.setFlexibilityOffered(myAttributeFlexibilityOffered);
							break;
						case "flexibility_accepted":
							AttributeOadrPayload myAttributeFlexibilityAccepted = new AttributeOadrPayload();
							myAttributeFlexibilityAccepted.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityAccepted = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityAccepted = payloadFloatTypeFlexibilityAccepted.getValue()
									.getValue();
							String myFlexibilityAccepted = Float.toString(valueFloatFlexibilityAccepted);
							log.debug("myFlexibilityAccepted: " + myFlexibilityAccepted);
							myAttributeFlexibilityAccepted.setValue(myFlexibilityAccepted);
							myEntityBidResult.setFlexibilityAccepted(myAttributeFlexibilityAccepted);
							break;
						case "reference_bid":
							AttributeOadrPayload myAttributeReferenceBid = new AttributeOadrPayload();
							myAttributeReferenceBid.setType("int");
							JAXBElement<PayloadFloatType> payloadFloatTypeReferenceBid = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatReferenceBid = payloadFloatTypeReferenceBid.getValue().getValue();
							String myReferenceBid = Integer.toString((int) valueFloatReferenceBid);
							log.debug("myReferenceBid: " + myReferenceBid);
							myAttributeReferenceBid.setValue(myReferenceBid);
							myEntityBidResult.setReferenceBid(myAttributeReferenceBid);
							break;

						}

					}

					if (Counters.existsBidResult()) {
						EntityBidResultOld myEntityBidResultOld = new EntityBidResultOld(myEntityBidResult.getResourceId(),
																						myEntityBidResult.getDateStart(),
																						myEntityBidResult.getDuration(),
																						myEntityBidResult.getFlexibilityPrice(),
																						myEntityBidResult.getFlexibilityOffered(),
																						myEntityBidResult.getFlexibilityAccepted(),
																						myEntityBidResult.getReferenceBid());
						
						patchEntityContextBroker(myEntityBidResultOld, myEntityBidResult.getId(), "/BidResult");
					} else {
						postEntityContextBroker(myEntityBidResult, "/BidResult");	
						Counters.setBidResult();
					}
					

				}

			}

		}

		log.debug("<- sendEntityBidResult");
		return;
	}
}
