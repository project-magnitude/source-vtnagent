package it.eng.oadr.ngsiagent.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import it.eng.oadr.ngsiagent.model.EntityOadrPayload;
import it.eng.oadr.ngsiagent.model.Notification;
import it.eng.oadr.ngsiagent.utilities.AgentProperties;
import it.eng.oadr.openadrmodel.Parser;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
@Path("CBtoVTN")
public class CBtoVTN {

	private static final Logger log = LoggerFactory.getLogger(CBtoVTN.class);

	@POST
	@Path("genericOADRPayload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized void postGenericOADRPayload(Notification myNotification) {
		log.debug("-> postGenericOADRPayload");

		String response = "";
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		String record = gson.toJson(myNotification);
		log.debug("myNotification: " + myNotification);

		EntityOadrPayload myEntity = myNotification.getData().get(0);
		// estrazione payload OADR
		String myType = myEntity.getType();
		log.debug("myType: " + myType);
		String myOadrPayload = decodeOadrPayload(myEntity.getOadrPayload().getValue());
		log.debug("decoded myOadrPayload: " + myOadrPayload);

		// SET URL Suffix
		String urlSuffix = "";
		switch (myType) {
		case "MsgEiRegisterPartyToVTN":
			urlSuffix = "/EiRegisterParty";
			break;
		case "MsgEiEventToVTN":
			urlSuffix = "/EiEvent";
			break;
		case "MsgEiReportToVTN":
			urlSuffix = "/EiReport";
			break;
		case "MsgEiOptToVTN":
			urlSuffix = "/EiOpt";
			break;
		case "MsgOadrPollToVTN":
			urlSuffix = "/OadrPoll";
			break;
		}
		log.debug("urlSuffix: " + urlSuffix);
		try {

			URL url = new URL(AgentProperties.getProperty("UrlVtnServer") + urlSuffix);

			log.debug("urlVTNServer: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/xml");
			connService.setRequestProperty("Content-Type", "application/xml");

			OutputStream os = connService.getOutputStream();
			os.write(myOadrPayload.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			response = sb.toString();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}

		switch (myType) {
		case "MsgEiRegisterPartyToVTN":
			new Simple20b().postEiRegisterParty(response);
			break;
		case "MsgEiEventToVTN":
			new Simple20b().postEiEvent(response);
			break;
		case "MsgEiReportToVTN":
			new Simple20b().postEiReport(response);
			break;
		case "MsgEiOptToVTN":
			new Simple20b().postEiOpt(response);
			break;
		case "MsgOadrPollToVTN":

			// new Simple20b().postOadrPoll(response);
			OadrPayload outPld = null;
			try {
				outPld = (OadrPayload) Parser.unmarshal(response);
			} catch (JAXBException ex) {
				log.error(ex.getMessage(), ex);
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
			
			OadrSignedObject outSiOb = outPld.getOadrSignedObject();
			if (outPld != null && outPld.getOadrSignedObject() != null) {
				outSiOb = outPld.getOadrSignedObject();
				if (outSiOb.getOadrResponse() != null) {
					log.debug("response -> OadrResponse");
					new Simple20b().postOadrPoll(response);

				}
				if (outSiOb.getOadrDistributeEvent() != null) {
					log.debug("response -> OadrDistributeEvent");
					new Simple20b().postEiEvent(response);

				}
				if (outSiOb.getOadrCancelPartyRegistration() != null) {
					log.debug("response -> OadrCancelPartyRegistration");
					new Simple20b().postEiRegisterParty(response);

				}
				if (outSiOb.getOadrRequestReregistration() != null) {
					log.debug("response -> OadrRequestReregistration");
					new Simple20b().postEiRegisterParty(response);
				}
				if (outSiOb.getOadrCancelReport() != null) {
					log.debug("response -> OadrCancelReport");
					new Simple20b().postEiReport(response);
				}
				if (outSiOb.getOadrRegisterReport() != null) {
					log.debug("response -> OadrRegisterReport");
					new Simple20b().postEiReport(response);
				}
				if (outSiOb.getOadrCreateReport() != null) {
					log.debug("response -> OadrCreateReport");
					new Simple20b().postEiReport(response);
				}
				if (outSiOb.getOadrUpdateReport() != null) {
					log.debug("response -> getOadrUpdateReport");
					new Simple20b().postEiReport(response);
				}
			}

			break;
		}

		log.debug("<- postGenericOADRPayload");
	}

	public synchronized String decodeOadrPayload(String oadrPayload) {
		log.debug("-> decodeOadrPayload");
		log.debug("oadrPayload: " + oadrPayload);
		String ret = oadrPayload;

		ret = ret.replace("#60$", "<");
		ret = ret.replace("#62$", ">");
		ret = ret.replace("#34$", "\"");
		ret = ret.replace("#39$", "\'");
		ret = ret.replace("#61$", "=");
		ret = ret.replace("#59$", ";");
		ret = ret.replace("#40$", "(");
		ret = ret.replace("#41$", "(");

		log.debug("<- decodeOadrPayload");
		return ret;
	}
}
