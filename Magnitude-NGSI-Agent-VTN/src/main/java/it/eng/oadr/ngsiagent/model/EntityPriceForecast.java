package it.eng.oadr.ngsiagent.model;

/**
 * Entity Model
 */
public class EntityPriceForecast extends EntityNGSI {

	private AttributeOadrPayload mesId;
	private AttributeOadrPayload forecastId;
	private AttributeOadrPayload dateStart;
	private AttributeOadrPayload duration;
	private AttributeOadrPayload uId;
	private AttributeOadrPayload price;

	public EntityPriceForecast() {
		super();
	}

	public EntityPriceForecast(String id, String type) {
		super(id, type);
	}

	public EntityPriceForecast(String id, String type, AttributeOadrPayload mesId, AttributeOadrPayload forecastId,
			AttributeOadrPayload dateStart, AttributeOadrPayload duration, AttributeOadrPayload uId,
			AttributeOadrPayload price) {
		super(id, type);
		this.mesId = mesId;
		this.forecastId = forecastId;
		this.dateStart = dateStart;
		this.duration = duration;
		this.uId = uId;
		this.price = price;
	}

	public AttributeOadrPayload getDateStart() {
		return dateStart;
	}

	public void setDateStart(AttributeOadrPayload dateStart) {
		this.dateStart = dateStart;
	}

	public AttributeOadrPayload getDuration() {
		return duration;
	}

	public void setDuration(AttributeOadrPayload duration) {
		this.duration = duration;
	}

	public AttributeOadrPayload getMesId() {
		return mesId;
	}

	public void setMesId(AttributeOadrPayload mesId) {
		this.mesId = mesId;
	}

	public AttributeOadrPayload getForecastId() {
		return forecastId;
	}

	public void setForecastId(AttributeOadrPayload forecastId) {
		this.forecastId = forecastId;
	}

	public AttributeOadrPayload getuId() {
		return uId;
	}

	public void setuId(AttributeOadrPayload uId) {
		this.uId = uId;
	}

	public AttributeOadrPayload getPrice() {
		return price;
	}

	public void setPrice(AttributeOadrPayload price) {
		this.price = price;
	}

}
